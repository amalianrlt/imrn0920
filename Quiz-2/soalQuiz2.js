//soal 1
class Score {
  constructor(subject, points, email) {
    this.subject = subject;
    this.points = points;
    this.email = email;
  }
  average = () => {
    if (typeof this.points === "number") {
      return this.points;
    } else if (typeof this.points === "object") {
      let total = 0;
      for (let i = 0; i < this.points.length; i++) {
        total += this.points[i];
      }
      return total / this.points.length;
    }
  };
  get summary() {
    return {
      email: this.email,
      subject: this.subject,
      points: this.average(this.points),
    };
  }
}

const totalScore = new Score("Math", [90, 76, 86], "nobody@mail.com");

totalScore.average();

//soal 3
function recapScores(data) {
  for (let i = 1; i < data.length; i++) {
    let sum = data[i][1] + data[i][2] + data[i][3];
    let average = sum / 3;
    if (average > 90) {
      let predicate = "honour";
    } else if (average > 80) {
      let predicate = "graduate";
    } else if (average > 70) {
      let predicate = "participant";
    }
    console.log(i + ". Email: " + data[i][0]);
    console.log("Rata-rata: " + average);
    console.log("predicate: " + predicate);
  }
}

recapScores(data);
