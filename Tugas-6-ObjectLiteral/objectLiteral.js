console.log("--------Tugas 2--------");
console.log();
function shoppingTime(memberId, money) {
  let shoppingCart = {};
  let stuff = [
    ["Sepatu Stacattu", 1500000],
    ["Baju Zoro", 500000],
    ["Baju H&N", 250000],
    ["Sweater Uniklooh", 175000],
    ["Casing Handphone", 50000],
  ];

  if (memberId === undefined && money === undefined) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }

  if (memberId === "") {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else {
    shoppingCart.memberId = memberId;
  }

  if (money <= 50000) {
    return "Mohon maaf, uang tidak cukup";
  } else {
    shoppingCart.money = money;
  }

  let totalPriceStuff = 0;
  shoppingCart.listPurchased = [];
  for (let i = 0; i < stuff.length; i++) {
    if (money > stuff[i][1]) {
      shoppingCart.listPurchased.push(stuff[i][0]);
      totalPriceStuff += stuff[i][1];
    }
    shoppingCart.changeMoney = money - totalPriceStuff;
  }
  return shoppingCart;
}

// TEST CASES
console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000));
console.log(shoppingTime("234JdhweRxa53", 15000));
console.log(shoppingTime());

console.log();
console.log();
console.log("--------Tugas 3--------");
console.log();
function naikAngkot(arrPenumpang) {
  rute = ["A", "B", "C", "D", "E", "F"];
  let angkot = [{}, {}];
  let i = 0;
  let asal = "";
  let tujuan = "";

  for (i; i < arrPenumpang.length; i++) {
    let j = 0;

    for (j; j < arrPenumpang[i].length; j++) {
      switch (j) {
        case 0: {
          angkot[i].penumpang = arrPenumpang[i][j];
          break;
        }
        case 1: {
          angkot[i].naikDari = arrPenumpang[i][j];
          angkot[i].tujuan = arrPenumpang[i][j + 1];
          break;
        }
        case 2: {
          asal = arrPenumpang[i][j - 1];
          tujuan = arrPenumpang[i][j];
          let jarak = 0;
          for (let k = 0; k < rute.length; k++) {
            if (rute[k] === asal) {
              for (let l = k + 1; l < rute.length; l++) {
                jarak += 1;
                if (rute[l] === tujuan) {
                  let bayar = jarak * 2000;
                  angkot[i].bayar = bayar;
                }
              }
            }
          }
          break;
        }
      }
    }
  }
  return angkot;
}
//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
