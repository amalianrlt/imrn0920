console.log('---------Tugas 1------------')
console.log()
const golden =  goldenFunction => {
  console.log("this is golden!!")
}
 
golden()

console.log()
console.log()
console.log('---------Tugas 2------------')
console.log()
const newFunction =  literal = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(firstName + " " + lastName)
      return 
    }
  }
}

// Driver Code 
newFunction("William", "Imoh").fullName() 

console.log()
console.log()
console.log('---------Tugas 3------------')
console.log()
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const wizard = {firstName, lastName, destination, occupation} = newObject
console.log(firstName, lastName, destination, occupation)

console.log()
console.log()
console.log('---------Tugas 4------------')
console.log()
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//Driver Code
let combined = [...west, ...east]
console.log(combined)

const planet = "earth"
const view = "glass"
let before = `lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim' ad minim veniam`

// Driver Code
console.log(before) 