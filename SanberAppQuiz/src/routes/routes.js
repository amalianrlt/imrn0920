import React from 'react';
import {View, Text} from 'react-native';
import SplashScreen from '../views/SplashScreen';
import RegisterScreen from '../views/RegisterScreen';
import LoginScreen from '../views/LoginScreen';
import HomeScreen from '../views/HomeScreen';

const RoutePage = () => {
  return (
    <View>
      {/* <SplashScreen /> */}
      {/* <RegisterScreen /> */}
      {/* <LoginScreen /> */}
      <HomeScreen />
    </View>
  );
};

export default RoutePage;
