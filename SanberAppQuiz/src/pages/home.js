import React from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  ScrollView,
  StatusBar,
} from 'react-native';
import Entypo from 'react-native-vector-icons/EvilIcons';
import Material from 'react-native-vector-icons/MaterialIcons';

const Home = () => {
  return (
    <View>
      <StatusBar backgroundColor="white" barStyle={'dark-content'} />
      <ScrollView style={{paddingHorizontal: 20}}>
        <View
          style={{
            justifyContent: 'space-between',
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <TextInput placeholder="Search Product" />
          <Entypo
            name="search"
            size={25}
            color="#3C5998"
            style={{marginRight: 10}}
          />
        </View>
        <Image
          source={require('../asset/Slider.png')}
          style={{width: 350, height: 150, borderRadius: 5}}
        />
        <View
          style={{
            justifyContent: 'space-between',
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            paddingTop: 20,
          }}>
          <Text>Man</Text>
          <Text>Woman</Text>
          <Text>Kids</Text>
          <Text>Home</Text>
        </View>
        <View
          style={{
            justifyContent: 'space-between',
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 20,
          }}>
          <Text style={{fontSize: 24, fontWeight: '400'}}>Flash Sale</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 18}}>All</Text>
            <Material name="navigate-next" size={20} color="orange" />
          </View>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{alignItems: 'center'}}>
            <Image
              source={require('../asset/Serum.png')}
              style={{height: 100, width: 100}}
            />
            <Text>Jbl speaker</Text>
            <Text>$12.22</Text>
          </View>
          <View style={{alignItems: 'center'}}>
            <Image
              source={require('../asset/Sp.png')}
              style={{height: 100, width: 100}}
            />
            <Text>Jbl speaker</Text>
            <Text>$12.22</Text>
          </View>
          <View style={{alignItems: 'center'}}>
            <Image
              source={require('../asset/Speaker.png')}
              style={{height: 100, width: 100}}
            />
            <Text>Jbl speaker</Text>
            <Text>$12.22</Text>
          </View>
        </View>
        <View
          style={{
            justifyContent: 'space-between',
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 20,
          }}>
          <Text style={{fontSize: 24, fontWeight: '400'}}>New Product</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 18}}>All</Text>
            <Material name="navigate-next" size={20} color="orange" />
          </View>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Image
            source={require('../asset/kursi.png')}
            style={{height: 170, width: 170}}
          />
          <Image
            source={require('../asset/sepatu.png')}
            style={{height: 170, width: 170}}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;
