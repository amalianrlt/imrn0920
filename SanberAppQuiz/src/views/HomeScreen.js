import React from 'react';
import {View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../pages/home';
import Profile from '../pages/profile';
import Cart from '../pages/cart';
import Message from '../pages/message';

const HomeScreen = () => {
  const Tab = createBottomTabNavigator();
  return (
    <View>
      <Tab.Navigator>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Cart" component={Cart} />
        <Tab.Screen name="Message" component={Message} />
        <Tab.Screen name="Profile" component={Profile} />
      </Tab.Navigator>
    </View>
  );
};
export default HomeScreen;
