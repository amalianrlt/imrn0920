import React, {useEffect} from 'react';
import {View, StatusBar, Image} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import logo from '../asset/logo.png';

const SplashScreenPage = () => {
  useEffect(() => {
    SplashScreen.hide();
  });
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'center',
        flexDirection: 'column',
      }}>
      <StatusBar backgroundColor="white" barStyle={'dark-content'} />
      <Image source={logo} alt="logo" />
    </View>
  );
};

export default SplashScreenPage;
