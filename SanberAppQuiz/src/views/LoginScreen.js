import React from 'react';
import {View, Text, TextInput, StatusBar, TouchableOpacity} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const LoginScreen = ({navigation}) => {
  const [value, onChangeText] = React.useState('Name');
  const UselessTextInput = (props) => {
    return (
      <TextInput
        {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
        editable
        maxLength={40}
      />
    );
  };
  return (
    <View
      style={{
        paddingHorizontal: 30,
        alignContent: 'center',
        // backgroundColor: 'blue',
        flexDirection: 'column',
        justifyContent: 'center',
        paddingTop: 90,
      }}>
      <StatusBar backgroundColor="white" barStyle={'dark-content'} />
      <Text style={{fontSize: 30, fontFamily: 'Montserrat', fontWeight: '700'}}>
        Welcome
      </Text>
      <Text style={{fontSize: 18, fontFamily: 'Montserrat', fontWeight: '400'}}>
        Sign in to continue
      </Text>
      <View
        style={{
          alignContent: 'center',
          // backgroundColor: 'red',
          flexDirection: 'column',
          justifyContent: 'center',
        }}>
        <View
          style={{
            backgroundColor: value,
            borderBottomColor: '#E6EAEE',
            borderBottomWidth: 1,
          }}>
          <UselessTextInput
            multiline
            numberOfLines={4}
            onChangeText={(text) => onChangeText(text)}
            value={'Email'}
          />
        </View>
        <View
          style={{
            backgroundColor: value,
            borderBottomColor: '#E6EAEE',
            borderBottomWidth: 1,
          }}>
          <UselessTextInput
            multiline
            numberOfLines={4}
            onChangeText={(text) => onChangeText(text)}
            value={'Password'}
          />
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('Home')}
          style={{
            marginVertical: 20,
            backgroundColor: '#F87866',
            // width: 120,
            paddingVertical: 23,
            flex: 1,
            borderRadius: 5,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              alignContent: 'center',
              justifyContent: 'center',
              fontSize: 20,
              fontFamily: 'Montserrat',
              fontWeight: '400',
              color: 'white',
            }}>
            Sign in
          </Text>
        </TouchableOpacity>
        <Text
          style={{
            fontSize: 16,
            fontFamily: 'Montserrat',
            fontWeight: '400',
            textAlign: 'center',
          }}>
          -OR-
        </Text>
        <View
          style={{
            justifyContent: 'space-between',
            // flex: 1,
            // padding:10,
            flexDirection: 'row',
          }}>
          <Text>
            <Entypo
              name="facebook"
              size={25}
              color="#3C5998"
              style={{marginRight: 10}}
            />
            Facebook
          </Text>
          <Text>
            {' '}
            <Entypo
              name="google-"
              size={25}
              color="#3C5998"
              style={{marginRight: 10}}
            />
            Google
          </Text>
        </View>
      </View>
    </View>
  );
};

export default LoginScreen;
