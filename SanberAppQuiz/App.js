import React from 'react';
import {View} from 'react-native';
import RoutePage from './src/routes/routes';

const App = () => {
  return (
    <View>
      <RoutePage />
    </View>
  );
};

export default App;
