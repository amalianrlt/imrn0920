console.log("--------Tugas 1--------");
console.log();
const teriak = () => {
  console.log("Halo Sanbers!");
};
teriak();

console.log();
console.log();
console.log("--------Tugas 2--------");
console.log();
var num1 = 12;
var num2 = 4;
function kalikan(num1, num2) {
  var hasil;
  hasil = num1 * num2;
  return hasil;
}
var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48

console.log();
console.log();
console.log("--------Tugas 3--------");
console.log();

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

function introduce(name, age, address, hobby) {
  return introduce;
}

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!"

