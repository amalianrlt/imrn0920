console.log('--------Tugas 1--------');
console.log();
let count = 2;
while (count < 20) {
    console.log(count, "- I love coding");
    count +=2;
}

console.log('-------------------');
let hitung = 20;
while (hitung > 2) {
    console.log(hitung, "- I will become a mobile developer");
    hitung -=2;
}

console.log();
console.log();
console.log();
console.log('--------Tugas 2--------');
console.log();
for (let angka = 1; angka <= 20; angka++){
    if (angka % 3 === 0 && (angka%2)==1  ){
        console.log(angka, "- I Love Coding");
    } else if ((angka%2)==1) {
        console.log(angka, "- Santai");
    } else if ((angka%2)===0){
        console.log(angka, "- Berkualitas");
    } else
    console.log (angka);
}

console.log();
console.log();
console.log();
console.log('--------Tugas 3--------');
console.log();

let rectangle = 8;
for (let i = 0; i < rectangle; i++) {
let sum = '';
	for (let j = 0; j < rectangle; j++) {
		if (i % 2 === 0) {
			sum += '#';
		} 
	}
	console.log(sum);
}

console.log();
console.log();
console.log();
console.log('--------Tugas 4--------');
console.log();

let stair = 7;
for (let i = 1; i <= stair; i++) {
	let sum = '';
	for (let j = 1; j <= i; j++) {
		
    sum += '#';
	}
	console.log(sum);
}

console.log();
console.log();
console.log();
console.log('--------Tugas 5--------');
console.log();

let sharp = "#";
let space = " ";
let sum = "";
let num = 8;
for (let i = 0;i < num;i++) {
  for (let j = 0;j < num;j++) {
    if (i % 2 == 1) {
      if (j % 2 == 0) {
        sum += sharp; 
      } else {
        sum += space; 
      }
    } else {
      if (j % 2 == 0) {
        sum += space;
      } else {
        sum += sharp; 
      }
    }
  }
  console.log(sum); 
  sum = " ";
}
