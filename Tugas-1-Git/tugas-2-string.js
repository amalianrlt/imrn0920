//Tugas String
//Nomer 1
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var kalimat = (`${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}`)
console.log(kalimat)


//Nomer 2
var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0]; 
var secondWord = sentence[2] + sentence[3]; 
var thirdWord = sentence.substr(5,5);
var fourthWord = sentence.substr(11,2);
var fifthWord = sentence.substr(14,2);
var sixthWord = sentence.substr(17,5);
var seventhWord = sentence.substr(23,6);
var eighthWord = sentence.substr(30,9);

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)


//Nomer 3
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(18, 15); 
var fourthWord2 = sentence2.substring(20, 18);
var fifthWord2 = sentence2.substring(25, 21);

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);


//Nomer 4
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substr(4,10); // do your own! 
var thirdWord3 = sentence3.substr(15,2); // do your own! 
var fourthWord3 = sentence3.substr(18,2); // do your own! 
var fifthWord3 = sentence3.substr(21,4); // do your own! 

var firstWordLength = exampleFirstWord3.length  
var secondWordLength = exampleFirstWord3.length  
// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3.length);
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3.length); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3.length);
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3.length);

//Tugas Conditional
//Nomer1
let nama;
let peran;

function cekNamaPeran(nama, peran) {
    let greeting = 'Selamat datang di Dunia Werewolf, ' + nama;
    let sapa = 'Halo '+ peran + ' ' + nama +', ';
    let pesanPenyihir = sapa + 'Kamu dapat melihat siapa yang menjadi werewolf!';
    let pesanGuard = sapa + 'Kamu akan membantu melindungi temanmu dari serangan werewolf!';
    let pesanWerewolf = sapa + 'Kamu akan memakan mangsa setiap malam!';

    if (nama === '' && peran === '') {
        return 'Nama harus diisi!';
    } else if (nama !== ''){
        if (peran === "") {
            return 'Halo ' + nama + ',' + ' Pilih peranmu untuk memulai game !';
        } else if (peran === 'Guard'){
            return greeting + '\n'+ pesanGuard;
        } else if (peran === 'Werewolf') {
            return greeting + '\n'+ pesanWerewolf;
        } else if ( peran === 'Penyihir'){
            return greeting + '\n'+ pesanPenyihir;
        }
    } 
}

console.log(cekNamaPeran('',''));
console.log('----------------');
console.log(cekNamaPeran('John',''));
console.log('----------------');
console.log(cekNamaPeran('Jane','Penyihir'));
console.log('----------------');
console.log(cekNamaPeran('Jenita','Guard'));
console.log('----------------');
console.log(cekNamaPeran('Junaedi','Werewolf'));
console.log('----------------');

//Nomer 2
console.log("I'm sorry I have no idea to write this code")